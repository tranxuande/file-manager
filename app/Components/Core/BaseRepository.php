<?php

/**
 * Created by PhpStorm.
 * User: darryldecode
 * Date: 5/6/2018
 * Time: 11:20 AM
 */

namespace App\Components\Core;

use App\Components\Core\Utilities\Helpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder;


abstract class BaseRepository
{
    /**
     * @var static|mixed Model
     */
    protected $model;

    /**
     * BaseRepository constructor.
     * @param Model $model
     */
    public function __construct($model)
    {
        $this->model = $model;
    }

    /**
     * @param array $params
     * @param array $with
     * @param callable $callable
     * @return LengthAwarePaginator|\Illuminate\Contracts\Pagination\LengthAwarePaginator|object
     */
    public function get($params = [], $with = [], callable $callable = null)
    {
        $q = $this->model->select($params['select'] ?? '*')->with($with);

        if (!Helpers::hasValue($params['order']) || $params['order'] !== 'no') $q->orderBy($params['order_by'] ?? 'id', $params['order_sort'] ?? 'desc');

        if (is_callable($callable)) $q = call_user_func_array($callable, [&$q]);

        // if per page is -1, we don't need to paginate at all, but we still return the paginated
        // data structure to our response. Let's just put the biggest number we can imagine.
        if (Helpers::hasValue($params['per_page']) && ($params['per_page'] == -1)) $params['per_page'] = 999999999999;
        if (Helpers::hasValue($params['trashed']) && ($params['trashed'] == 'yes')) $q->withTrashed();

        // if don't want any pagination
        if (Helpers::hasValue($params['paginate']) && ($params['paginate'] == 'no')) {
            if (Helpers::hasValue($params['more']) && $params['more'] == 'no') return $q->first();
            if (Helpers::hasValue($params['limit'])) $q->limit($params['limit']);
            if (Helpers::hasValue($params['offset'])) $q->offset($params['offset']);
            return $q->get();
        }

        return $q->paginate($params['per_page'] ?? 10);
    }

    /**
     * @param array $data
     * @return Model|boolean
     */
    public function create(array $data = [])
    {
        return $this->model->create($data);
    }

    /**
     * @param array $data
     * @return Model|boolean
     */
    public function insert(array $data = [])
    {
        return $this->model->insert($data);
    }
    /**
     * @param int $id
     * @param array $attributes
     * @return bool
     */
    public function update(int $id, array $attributes)
    {
        $model = $this->find($id);

        if (!$model) return false;

        return $model->update($attributes);
    }

    /**
     * @param int $id
     * @param array $attributes
     * @param Model $model
     * @return mixed
     */
    public function fillUpdate(int $id, array $attributes, $model = null)
    {
        if (!$model) $model = $this->find($id);

        if (!$model) return false;
        $model->fill($attributes);
        $model->save();

        return $model;
    }

    /**
     * update more in table
     *
     * @param array $attributes
     * @param string $index
     * @return mixed
     */
    public function updateMore($index = 'id', $attributes = [])
    {
        return \Batch::update(new $this->model, $attributes, $index);
    }

    /**
     * update more data by raw in table
     *
     * @param $params $attributes
     * @param string $raw
     * @return mixed
     */
    public function updateRaw($raw = '', $params = [])
    {
        //        DB::connection('mysql')->update("UPDATE products SET status = ? where status = ?", [1, 0]);
        return DB::connection('mysql')->update($raw, $params);
    }

    /**
     * @param int $id
     * @return bool|null
     * @throws \Exception
     */
    public function delete(int $id)
    {
        return $this->model->delete($id);
    }

    /**
     * @param  int    $id
     * @param  array $params
     * @return bool|null
     */
    public function deleteWithCondition(int $id, $params)
    {
        $this->model->where('id', $id)
            ->where($params)
            ->delete();
    }

    /**
     * @param  array|callable $params
     * @return bool
     */
    public function findDelete($params)
    {
        return $this->model
            ->where($params)
            ->delete();
    }
    /**
     * @param int $id
     * @param array $with
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|Model|Model[]|null|static
     */
    public function find(int $id, $with = [])
    {
        return $this->model->with($with)->find($id);
    }

    /**
     * @param int $id
     * @return mixed|Model|boolean
     */
    public function findWithTrash(int $id)
    {
        return $this->model->withTrashed()->find($id);
    }

    /**
     * @param mixed $value
     * @return mixed|Model|static
     */
    public function findBy($value)
    {
        return $this->model->where($value)->first();
    }

    /**
     * @param mixed $value
     * @return mixed|Model|static
     */
    public function findByWithTrash($value)
    {
        return $this->model->where($value)->withTrashed()->first();
    }

    /**
     * count data
     *
     * @param array $params
     * @param callable $callable
     * @return mixed
     */
    public function count($params = [], $callable = null)
    {
        $q = $this->model->select('*');

        if (is_callable($callable)) $q = call_user_func_array($callable, [&$q]);
        return $q->count();
    }


    /**
     * count data
     *
     * @param array $params
     * @param array $attribute
     * @return mixed
     */
    public function updateOrCreate($attribute = [], $params = [])
    {
        return $this->updateOrCreate($attribute, $params);
    }

    public function paginate(int $perPage, array $relation = [], $column = ['*'])
    {
        // TODO: Implement paginate() method.
        return $this->model->with($relation)->orderBy('id', 'DESC')->paginate($perPage);
    }

    public function findByField($field, $value, array $relation = [])
    {
        return $this->model->where($field, $value)->with($relation)->get();
    }

    public function getBuilder(Builder $builder)
    {
        return $builder->get();
    }

    public function limit(Builder $builder, $limit)
    {
        return $builder->paginate($limit);
    }

    public function detail(int $id,  array $relation = [])
    {
        // TODO: Implement detail() method.
        return $this->model->with($relation)->find($id);
    }

    public function getById(int $id)
    {
        return $this->model->findOrFail($id);
    }

    public function multiDelete($data)
    {
        return $this->model->destroy($data);
    }
}
