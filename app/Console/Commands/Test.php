<?php

namespace App\Console\Commands;

use App\Helper\HelperCheckDomain;
use App\Helper\Helpers;
use App\Helper\HelperTeleGramBot;
use App\Models\Feature;
use App\Models\Order;
use App\Models\Website;
use Illuminate\Console\Command;

class Test extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'q';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

    }
}
