<?php

namespace App\Helper;

class Helpers
{
    public static function numberToMoney($number)
    {
        return number_format($number, 0, ',', '.');
    }

    public static function countMonthToDate($date)
    {
        $date = new \DateTime($date);
        $now = new \DateTime();
        $interval = $date->diff($now);
        return $interval->m + ($interval->y * 12);
    }
}
