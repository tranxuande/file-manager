<?php

namespace App\Http\Controllers;

use App\Service\FileService;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Components\Core\ResponseHelpers;
use App\Http\Requests\FileUploadRequest;
use Illuminate\Support\Facades\Validator;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Symfony\Component\Console\Input\Input;
use function GuzzleHttp\Promise\all;

class FileController extends Controller
{
    use ResponseHelpers;

    public $fileService;

    public function __construct(FileService $fileService)
    {
        $this->fileService = $fileService;
    }

    public function uploadFile(Request $request, $id)
    {
        return $this->fileService->uploadFile($request, $id);
    }

    public function renameFile(Request $request, $id)
    {
        $update = $this->fileService->renameFile($request, $id);

        if ($update) {
            return redirect()->back()->with(config('util.SUCCESS'), 'Cập nhập thành công !');
        }
        return redirect()->back()->with(config('util.ERROR'), 'Update thất bại !');
    }


    public function deleteFile(Request $request)
    {
        $arrId = $request->file_ids;

        return $this->fileService->deleteFile($arrId);
    }

    public function search(Request $request)
    {
        $id = $request->id;
        $value = $request->value;

        $searchFile = $this->fileService->search($id, $value);
        $returnThis = view('pages.folders.item_template', compact('searchFile'))->render();

        return response()->json(['status' => true, 'data' => $returnThis]);
        //return $this->fileService->search($id,$value);
    }

    public function copy(Request $request)
    {
        return $this->fileService->copy($request);
    }

    public function move(Request $request)
    {
//        $id_folder_move = $request->id_folder_move;
//        $id_file = $request->file_id;
//
//        $this->fileService->move($id_file, $id_folder_move);
//
//        return redirect()->back()->with(config('util.SUCCESS'), 'Move thành công !');
        //return $this->fileService->move($file_id,$id_folder_move);
        $arrId=explode(',',$request->file_id);

        $id_folder_move=$request->id_folder_move;

        $this->fileService->move($arrId,$id_folder_move);

        return redirect()->back();
    }
}
