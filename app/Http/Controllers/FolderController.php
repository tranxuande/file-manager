<?php

namespace App\Http\Controllers;

use App\Service\FolderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class FolderController extends Controller
{
    public $folderService;

    public function __construct(FolderService $folderService)
    {
        $this->folderService = $folderService;
    }


    /**
     * This function is used to get all the folders and return the view of the list of folders
     *
     * @return The view is being returned.
     */
    public function index()
    {
        $datas = $this->folderService->getListFolder();

        return view('pages.folders.list', compact('datas'));
    }


    /**
     * The above function is used to create a new folder.
     *
     * @param Request request The request object.
     * @param id The id of the folder you want to create a subfolder in.
     *
     * @return The return value of the last statement executed in the try block.
     */
    public function store(Request $request, $id)
    {
        $data = [
            'name' => $request->name,
            'parent_id' => $id,
            'user_id' => Auth::id(),
        ];

        $store = $this->folderService->store($data);

        if ($store) {
            return redirect()->route('folder.list')->with(config('util.SUCCESS'), 'Thêm thành công !');
        }
        // return $this->folderService->store($data);
        return redirect()->back()->with(config('util.ERROR'), 'Thêm thất bại !');
    }


    /**
     * Update folder
     * @param Request request The request object.
     * @param id The id of the folder to be updated.
     */
    public function update(Request $request, $id)
    {
        $update = $this->folderService->update($id, $request->name);

        if ($update) {
            return redirect()->route('folder.list')->with(config('util.SUCCESS'), 'Cập nhập thành công !');
        }

        return redirect()->back()->with(config('util.ERROR'), 'Cập nhập thất bại !');
        // return $this->folderService->update($id, $request->name);
    }

    public function show($id)
    {
        $folder = $this->folderService->detail($id);
        $folderMove = $this->folderService->folderForMove($id);

        return view('pages.folders.detail', compact('folder', 'id', 'folderMove'));
//        return $this->folderService->show($id);
    }


}
