<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\AuthRequest;
use App\Jobs\SendEmail;
use App\Models\User;
use App\Repository\FolderRepository;
use App\Repository\UserRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo = '/login';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $userRepository;
    public $folderRepository;

    public function __construct(UserRepository $userRepository, FolderRepository $folderRepository)
    {
        $this->userRepository = $userRepository;
        $this->folderRepository = $folderRepository;
        $this->middleware('guest:web')->except('logout');
    }

    public function guard()
    {
        return Auth::guard('web');
    }

    public function showLoginForm()
    {
        return view('user.auth.login');
    }

    public function login(AuthRequest $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');

        if ($this->guard()->attempt(['email' => $email, 'password' => $password])) {
            return redirect()->route('folder.list');
        } else {
            return redirect()->route('login')->with('error', 'Invalid account!');
        }
    }

    public function logout()
    {
        $this->guard()->logout();
        return redirect('/');
    }

    public function showRegisterForm()
    {
        return view('user.auth.register');
    }

    public function register(Request $request)
    {
        $request->validate([
            'email' => 'unique:users',
        ]);

        $data = $request->only(['email', 'password', 'name']);

        $check = $this->userRepository->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);

        if ($this->guard()->attempt(['email' => $data['email'], 'password' => $data['password']])) {

            $this->folderRepository->create([
                    'name' => 'Files',
                    'parent_id' => 0,
                    'user_id' => Auth::id()]
            );
            $this->folderRepository->create([
                    'name' => 'Images',
                    'parent_id' => 0,
                    'user_id' => Auth::id()]
            );

            return redirect()->route('folder.list');
        }
    }

    public function showForgetPasswordForm()
    {
        return view('user.auth.forgetPassword');
    }

    public function submitForgetPasswordForm(Request $request)
    {
        $email = $request->input('email');

        $data = [
            'type' => 'reset task',
            'content' => 'reset!',
            'email' => $email
        ];

        SendEmail::dispatch($data, $email)->delay(now()->addMinute(1));
        return back()->with('message', 'We have e-mailed your password reset link!');
    }

    public function showResetPasswordForm($email)
    {
        return view('user.auth.reset_password', compact('email'));
    }

    public function submitResetPasswordForm(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required'
        ]);

        $user = User::where('email', $request->email)
            ->update(['password' => Hash::make($request->password)]);


        return redirect('/')->with('message', 'Your password has been changed!');
    }

}
