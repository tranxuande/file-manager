<?php

namespace App\Http\Controllers\User;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePassController
{
    public function showFormChangePass()
    {
        return view('user.account.change_password');
    }

    public function postFormChangePass(Request $request)
    {
        $user = User::find(Auth::id());
        $user->password = Hash::make($request->password);
        $user->save();
        return redirect()->route('user.logout');
    }
}
