<?php

namespace App\Http\Controllers\User;

use App\Service\UserService;
use Illuminate\Routing\Controller;

class UserController extends Controller
{
    public $userService;
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }
    public function getAll()
    {
        $list=$this->userService->getAll();
        return view('user.account.list', compact('list'));
    }
}
