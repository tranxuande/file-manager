<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class EnsureEmailVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('web')->check() && Auth::guard('web')->user()->email_verified_at){
            return $next($request);
        }
        return redirect()->route('user.account.info')->with('error', __('Vui lòng xác thực email trước'));
    }
}
