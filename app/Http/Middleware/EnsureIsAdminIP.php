<?php

namespace App\Http\Middleware;

use App\Models\AdminIpAccepted;
use Closure;
use Illuminate\Support\Facades\Auth;

class EnsureIsAdminIP
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(AdminIpAccepted::query()->where('admin_ip_address',$request->ip())->first()){
            return $next($request);
        }
        if(Auth::guard('web')->check()) abort(404);
        return abort(404);
    }
}
