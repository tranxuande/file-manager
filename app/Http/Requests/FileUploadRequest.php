<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileUploadRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'files' => 'required',
            //'files.*' => 'image|mimes:*'
        ];
    }
    public function message()
    {
        return [
            'files.required' => 'Trường này không được để trống.',
            'files.*.image' => 'Vui lòng tải lên đúng định dạng ảnh.',
        ];
    }
}
