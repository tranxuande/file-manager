<?php

namespace App\Jobs;

use App\Mail\MailNotify;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $data, $user;

    public function __construct($data, $email)
    {
        $this->data = $data;
        $this->user = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
       // Mail::to($this->user)->send(new MailNotify($this->data));
    }

}
