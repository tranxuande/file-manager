<?php

namespace App\Models;

use App\Casts\FormatSizeFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class File extends Model
{
    protected $table = "files";
    protected $primaryKey = "id";
    protected $guarded = [];

    protected $casts = [
        'size' => FormatSizeFile::class,
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('created_at', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }
}
