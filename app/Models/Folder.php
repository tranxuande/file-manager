<?php

namespace App\Models;

use App\Models\File;
use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    protected $table = "folders";
    protected $primaryKey = "id";
    protected $guarded = [];

    public function files()
    {
        return $this->hasMany(File::class,  'folder_id');
    }
    function childrens()
    {
        return $this->hasMany(Folder::class, 'parent_id')->with(['childrens' => function ($q) {
            return $q->withCount(['childrens']);
        }]);
    }
}
