<?php

namespace App\Providers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->app->bind('FeatureService', FeatureService::class);
        // $this->app->bind('OrderRepository', OrderRepository::class);
        // $this->app->bind('WebsiteRepository', WebsiteRepository::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // view()->composer('admin.layouts.sidebar', function ($view) {
        //     $view->with([
        //         'watting_confirm_feature' => app('FeatureService')->getAmountWattingConfirmedFeature(),
        //         'watting_for_quote_order' => app('OrderRepository')->getAmountWattingForQuote(),
        //         'watting_for_money_transfer_order' => app('OrderRepository')->getAmountWattingForMoneyTransfer(),
        //         'watting_website_confirm_complete' => app('WebsiteRepository')->getAmountWattingConfirmComplete(),
        //         'watting_website_confirm_feature_addition_complete' => app('WebsiteRepository')->getAmountWattingConfirmFeatureAdditionComplete(),
        //         'watting_for_payment_user' => app('OrderRepository')->getAmountWattingForPaymentUser(Auth::guard('web')->id()),
        //     ]);
        // });

        // view()->composer('user.layouts.sidebar', function ($view) {
        //     $view->with([
        //         'watting_for_payment_user' => app('OrderRepository')->getAmountWattingForPaymentUser(Auth::guard('web')->id()),
        //     ]);
        // });
    }
}
