<?php

/**
 * Created by VSCode.
 * User: trong
 * Date: 13/12/2022
 * Time: 3:00 PM
 */

namespace App\Providers;

use App\Service\FileService;
use App\Service\FolderService;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('FolderService', FolderService::class);
        $this->app->bind('FileService', FileService::class);
    }
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
