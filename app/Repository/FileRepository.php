<?php

/**
 * Created by VSCode.
 * User: trong
 * Date: 13/12/2022
 * Time: 3:00 PM
 */

namespace App\Repository;

use App\Components\Core\BaseRepository;
use App\Models\File;

class FileRepository extends BaseRepository
{
    public function __construct(File $model)
    {
        parent::__construct($model);
    }

    public function search($idFolder, $value)
    {
        return $this->model
            ->where('name', 'like', '%' . $value . '%')
            ->where('folder_id', $idFolder)
            ->get();
    }

    public function moveMulti($arrId,$folder_id)
    {
        return $this->model->whereIn('id', $arrId)
            ->update(['folder_id' => $folder_id]);
    }

    public function findFileById(int $id)
    {
        return $this->model->where('id', $id)->first();
    }
}
