<?php

/**
 * Created by VSCode.
 * User: trong
 * Date: 13/12/2022
 * Time: 3:00 PM
 */

namespace App\Repository;

use App\Models\Folder;
use App\Components\Core\BaseRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class FolderRepository extends BaseRepository
{
    public function __construct(Folder $model)
    {
        parent::__construct($model);
    }

    public function getListFolder()
    {
        return $this->model
            ->where(
                [
                    'parent_id' => 0,
                    'user_id' => Auth::id()
                ]
            )
            ->with(['childrens' => function ($q) {
                return $q->withCount(['childrens']);
            }])
            ->withCount(['childrens'])->get();
    }

    public function getall()
    {
        return $this->model
            //->select('id','parent_id','name')
            ->select(DB::raw('id,parent_id,name as title,level'))
            ->where('user_id',Auth::id())
            ->get();
    }

    public function getListFolderForMove($id)
    {
        return $this->model->select('id', 'name')
            ->whereNotIn('id', [$id])
            ->where('user_id', Auth::id())
            ->get();
    }
}
