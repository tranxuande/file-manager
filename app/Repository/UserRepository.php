<?php
/**
 * Created by PhpStorm.
 * User: darryl
 * Date: 10/6/2017
 * Time: 6:37 AM
 */

namespace App\Repository;

use App\Components\Core\BaseRepository;
use App\User;

class UserRepository extends BaseRepository
{
    public function __construct(User $model){
        parent::__construct($model);
    }

    public function getAll()
    {
        return $this->model->select('name', 'email')->get();
    }
}
