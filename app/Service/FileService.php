<?php

/**
 * Created by VSCode.
 * User: trong
 * Date: 13/12/2022
 * Time: 3:00 PM
 */

namespace App\Service;

use App\Components\Core\ResponseHelpers;
use App\Repository\FileRepository;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Pion\Laravel\ChunkUpload\Handler\HandlerFactory;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

class FileService
{
    use ResponseHelpers;

    private $fileRepository;

    public function __construct(FileRepository $fileRepository)
    {
        $this->fileRepository = $fileRepository;
    }

    public function uploadFile($request, $id)
    {
        $receiver = new FileReceiver('file', $request, HandlerFactory::classFromRequest($request));

        if (!$receiver->isUploaded()) {
            return;
        }

        $fileReceived = $receiver->receive();
        if ($fileReceived->isFinished()) {
            $file = $fileReceived->getFile();
            $fileName = $file->getClientOriginalName();
            Storage::disk(config('util.DISK_STORAGE_DEFAULT'))->putFileAs('', $file, $fileName);
            $url = Storage::disk(config('util.DISK_STORAGE_DEFAULT'))->url($fileName);

            $this->fileRepository->create([
                'name' => $fileName,
                'size' => $file->getSize(),
                'type_file' => $file->getClientOriginalExtension(),
                'path' => $url,
                'user_id' => Auth::id(),
                'folder_id' => $id
            ]);
            unlink($file->getPathname());
            return $this->sendResponseOk([], 'Tải tệp lên thành công !');
        }
    }

    public function renameFile($request, $id)
    {
        $file = $this->fileRepository->findBy(['id' => $id]);
        $name = explode('.', $request->name)[0] . "." . $file->type_file;

        if ($file->name != $name) {
            Storage::disk(config('util.DISK_STORAGE_DEFAULT'))->move($file->name, $name);
            $this->fileRepository->update($id,
                [
                    'name' => $name,
                    'path' => Storage::disk(config('util.DISK_STORAGE_DEFAULT'))->url($name)
                ]);
            return true;

        }
        return false;
    }


    public function deleteFile($arrId)
    {
        DB::beginTransaction();
        try {
            foreach ($arrId as $id) {
                $file = $this->fileRepository->findBy(['id' => $id]);
                if ($file) {
                    Storage::disk(config('util.DISK_STORAGE_DEFAULT'))->delete($file->name);
                    $file->delete();
                }
            }
            DB::commit();
            return response()->json(['code' => 200, 'msg' => 'Delete sucess']);
        } catch (\Exception $e) {
            DB::rollBack();
        }
        return ;
    }

    public function search($idFolder, $value)
    {
        return $this->fileRepository->search($idFolder, $value);

    }

    public function copy($request)
    {
        $file = $this->fileRepository->findBy(['id' => $request->id]);
        $copyFile = $file->replicate();
        $newName = 'Copy of ' . $file->name;
        Storage::disk(config('util.DISK_STORAGE_DEFAULT'))->copy($file->name, $newName);
        $copyFile->name = $newName;
        $copyFile->save();
        return response()->json(['code' => 200]);
    }

    public function move($id, $folder_id)
    {
        DB::beginTransaction();
        try {
            $this->fileRepository->moveMulti($id, $folder_id);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }

    }
}
