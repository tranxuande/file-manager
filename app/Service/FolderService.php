<?php

/**
 * Created by VSCode.
 * User: trong
 * Date: 13/12/2022
 * Time: 3:00 PM
 */

namespace App\Service;

use App\Repository\FolderRepository;
use App\Repository\UserRepository;
use Illuminate\Support\Facades\DB;

class FolderService
{

    public $folderRepository;
    public function __construct(FolderRepository $folderRepository)
    {
        $this->folderRepository = $folderRepository;
    }

    public function getListFolder()
    {
       return  $this->folderRepository->getListFolder();
    }
    public function getAll()
    {
        return $this->folderRepository->getall();
    }

    public function store($data)
    {
        try {
            $this->folderRepository->create([
                'name' => $data['name'],
                'parent_id' => $data['parent_id'],
                'user_id' => $data['user_id']
            ]);
            return true;

        } catch (\Throwable $th) {

        }
        return false;
    }

    public function update($id, $name)
    {
        try {
            $this->folderRepository->update($id, ['name' => $name]);
            return true;

        } catch (\Throwable $th) {

        }
        return false;
    }

    public function detail($id){
        return $this->folderRepository->detail($id,['files']);
    }

    public function folderForMove($id){
        return $this->folderRepository->getListFolderForMove($id);
    }

}
