<?php

namespace App;

use App\Jobs\SendResetPasswordQueue;
use App\Jobs\SendVerifiesEmailQueue;
use App\Models\UserWalletAddress;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //Make Verify Email Queue
    public function sendEmailVerificationNotification()
    {
        SendVerifiesEmailQueue::dispatch($this);
    }

    public function sendPasswordResetNotification($token)
    {
        SendResetPasswordQueue::dispatch($this, $token);
    }
}
