<?php
return [
    'allowed_domain_extensions' => ['com','net','org','xyz','link','vn'],
    'web_hosting_id' => 1,
    'vn_domain_id' => 10,
    'com_domain_id' => 11,
    'accept_discount_type' => [1,2,3,4],
    'accept_months_registration' => [12,24,48],
    'discount_type' => [
        1 => [
            'description' => 'Web Hosting',
            'condition' => [
                1 => [
                    12 => 37,
                    24 => 44,
                    48 => 52,
                ],
                3 => [
                    12 => 16,
                    24 => 21,
                    48 => 34,
                ]
            ]
        ],
        2 => [
            'description' => 'Domain (vn, com.vn, ...)',
            'condition' => [
                1 => [
                    12 => 0,
                    24 => 0,
                    48 => 0,
                ],
                3 => [
                    12 => 0,
                    24 => 0,
                    48 => 0,
                ]
            ]
        ],
        3 => [
            'description' => 'Domain (com, net, ...)',
            'condition' => [
                1 => [
                    12 => 100,
                    24 => 50,
                    48 => 25,
                ],
                3 => [
                    12 => 0,
                    24 => 0,
                    48 => 0,
                ]
            ]
        ],
        4 => [
            'description' => 'Feature',
            'condition' => [
                12 => 0,
                24 => 10,
                48 => 20,
            ]
        ],
    ],
];
