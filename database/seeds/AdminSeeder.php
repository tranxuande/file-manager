<?php

use Illuminate\Database\Seeder;
use App\Models\User;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'email' => "admin@gmail.com",
            'email_verified_at' => now(),
            'password' => '$2a$12$gBZhVV1/AOnhAlSEt6uRSO2t4ZJeK6EhnEAtu7a09U.L7J0tjJ/N6', // 123456
            'remember_token' => Str::random(10),
        ])->assignRole('admin');
    }
}
