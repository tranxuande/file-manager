


  <div>
            <div class="popup" id="myPopup">
                <div class="popup-wrapper">
                    <button class="popup_button" type="submit">X</button>
                    <div class="popup_content">
                        <div class="form-group row">
                            <h5>{{ __('Drag and drop multipe files') }}</h5>
                        </div>
                        <div id="uploaderHolder">
                            <form action="{{ route('file-upload') }}"
                                  class="dropzone"
                                  id="datanodeupload">
                                @csrf
                                <input type="file" name="file"  style="display: none;">
                                <input type="hidden" name="dataTS" id="dataTS" value="">
                                <input type="hidden" name="dataDATE" id="dataDATE" value="">

                            </form>
                        </div>
                        <button type="button" class="btn btn-primary" id="btn-upload">{{ __('Save') }}</button>
                    </div>
                </div>
            </div>
        </div>