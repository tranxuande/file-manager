CKEDITOR.plugins.setLang( 'toc', 'vi', {
    tooltip : 'Tạo Mục Lục',
    notitles: 'Không có tiêu đề nào có sẵn! \nVui lòng định dạng một số văn bản là "Heading", "Heading 2", v.v. trong trình chỉnh sửa',
    ToC: 'Mục Lục'
});
