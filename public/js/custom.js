var Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});

$(function () {
    //Add text editor
    $('#compose-textarea').summernote({
        placeholder: 'Nội dung',
        tabsize: 2,
        height: 300,
        toolbar: [
            ['style', ['style']],
            ['font', ['bold', 'italic', 'underline', 'clear']],
            ['fontname', ['fontname']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph', 'height']],
            ['table', ['table']],
            ['insert', ['link', 'picture', 'video', 'hr']],
            ['view', ['fullscreen', 'codeview', 'undo', 'redo', 'help']],
        ],
    })
    $('.select2').select2();
    $('.select2bs4').select2({
        theme: 'bootstrap4'
    });
    bsCustomFileInput.init();
})

var UploadImage = {
    'ISSET_ID' : []
}

function uploadImage(url, token, element_id, callback){
    if($.inArray( element_id, UploadImage.ISSET_ID ) > -1) return;
    UploadImage.ISSET_ID.push(element_id)
    // Upload Image
    if(!url || !token) return false;
    var uploader = new plupload.Uploader({
        runtimes: 'html5, html4, Flash, Silverlight',
        url: url,
        max_file_size : '1000mb',
        chunk_size: '1mb',
        browse_button : element_id,
        filters: {
            prevent_duplicates: true,
            mime_types: [{
                title: 'File',
                extensions: 'jpg,jpeg,gif,png'
            }]
        },
        init: {
            BeforeUpload: function(up, file) {
                up.settings.multipart_params = {
                    "_token" : token,
                };
                show_loading();
                Alert('Đang upload.....', 1);
            },
            Error: function(up, err) {
                hide_loading();
                if(err.status){
                    Alert(JSON.parse(err.response).message, 2);
                }else {
                    Alert(err.message, 2);
                }
            },
            FilesAdded:  function(up, files) {
                plupload.each(files, function(file) {
                    if (up.files.length > 1) {
                        Alert('Số file đã tối đa', 2);
                        up.removeFile(file);
                    }else{
                        uploader.start();
                    }
                });
            },
            FileUploaded: function(up, file, result) {
                hide_loading();
                up.splice();
                callback(JSON.parse(result.response).data.path);
                Alert('Upload file thành công!!', 1);
            },
        },
        multi_selection: false,
        multiple_queues: true,
        rename: true,
        sortable: true,
        dragdrop: true,
        flash_swf_url: '/js/plupload/files/Moxie.swf',
        silverlight_xap_url: '/js/plupload/files/Moxie.xap',
    });

    uploader.init();
    //End Upload Image
}

// Upload image imgur
function uploadImgur(file, callback) {
    return $.ajax({
        url: 'https://api.imgur.com/3/image',
        type: 'post',
        data: file,
        headers: {
            "Authorization": "Client-ID f270e0ea6195a51",
        },
        processData: false,
        contentType: 'application/octet-stream',
        beforeSend: Alert('Uploading....', 1),
        success: function (data) {
            Alert('Upload ảnh thành công', 1);
            callback(data.data.link);
        },
        error: function (data) {
            Alert('Upload thất bại', 2);
        }
    });
}

function Alert(title, type) {
    if(type === 1) toastr.success(title)
    if(type === 2) toastr.error(title)
    if(type === 3) toastr.warning(title)
    if(type === 4) toastr.info(title)
}

function ConfirmForm(title, text, confirm_button_text, form = 'form'){
    Swal.fire({
        title: title,
        text: text,
        showCancelButton: true,
        confirmButtonColor: '#24943D',
        cancelButtonColor: '#d33',
        confirmButtonText: confirm_button_text,
        cancelButtonText: 'Hủy',
    }).then((result) => {
        if (result.value) {
            $(form).submit();
        }
    });
}

function delayKeyup(callback, ms) {
    var timer = 0;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timer);
        timer = setTimeout(function () {
            callback.apply(context, args);
        }, ms || 0);
    };
}

function show_loading(){
    $.blockUI({
        message: '<div class="theme_corners">\n' +
            '\t\t\t\t\t\t\t\t\t\t<i class="fas fa-cog fa-spin"></i>\n' +
            '\t\t\t\t\t\t\t\t\t</div>',
        timeout: 0, //unblock after 2 seconds
        overlayCSS: {
            backgroundColor: '#1b2024',
            opacity: 0.8,
            zIndex: 1200,
            cursor: 'wait'
        },
        css: {
            border: 0,
            color: '#fff',
            padding: 0,
            zIndex: 1201,
            backgroundColor: 'transparent'
        }
    });
}

function hide_loading(){
    $.unblockUI();
}
