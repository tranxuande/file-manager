@extends('user.layouts.master')
@section('title', 'Quản lý thư mục')
@section('content_title', "Chi tiết thư mục : $folder->name")
@section('page-style')
    <style>
        .card-footer,
        .progress {
            display: none;
        }
    </style>
@endsection
@section('content')
    <div class="card p-3">
        <div class="row mb-3">
            <div class="col-8">
                <button type="button" id="browseFile" class="btn btn-primary">
                    <svg xmlns="http://www.w3.org/2000/svg"
                         width="16" height="16" fill="currentColor" class="bi bi-upload" viewBox="0 0 16 16">
                        <path
                            d="M.5 9.9a.5.5 0 0 1 .5.5v2.5a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1v-2.5a.5.5 0 0 1 1 0v2.5a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2v-2.5a.5.5 0 0 1 .5-.5z"/>
                        <path
                            d="M7.646 1.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 2.707V11.5a.5.5 0 0 1-1 0V2.707L5.354 4.854a.5.5 0 1 1-.708-.708l3-3z"/>
                    </svg>
                    Tải tệp lên
                </button>
                <div class="progress  w-100" style="height: 38px">
                    <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar"
                         aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%; height: 100%">75%
                    </div>
                </div>
            </div>
            <div class="col-4 d-flex ">
                <input placeholder="search" style="margin-right: 20px" type="text" id="search"
                       class="form-control mr-sm-2">
                <button type="button" id="browseFile" class=" btn btn-primary delete_file" style="display: none">
                    <i class="fa fa-trash" aria-hidden="true"></i>
                </button>
                <button type="button" id="browseFile" class=" btn btn-primary move_file"
                        style="display: none; margin-left: 5px">
                    Move
                </button>
            </div>
        </div>
        <div>
            @error('files')
            {{ $message }}
            @enderror
            @error('files.*')
            {{ $message }}
            @enderror
        </div>
        <div>
            <input type="checkbox" class="checkbox" id="all">
        </div>

        <div class="row" id="counties-table">
            @foreach ($folder->files as $file)
                <div class="col-3 card_file" data-file="{{$file}}" data-url="{{route('file.rename',$file->id)}}">
                    <div class=" card  text-white bg-info card-file ">
                        <input type="checkbox" class="checkbox-select" >
                        @switch($file->type_file)
                            @case('doc')
                                <div class="icon-file"><i class="fa fa-file-word"></i></div>
                                @break
                            @case('docx')
                                <div class="icon-file"><i class="fa fa-file-word"></i></div>
                                @break
                            @case('xlsx')
                                <div class="icon-file"><i class="fa fa-solid fa-file-excel"></i></div>
                                @break
                            @case('zip')
                                <div class="icon-file"><i class="fa fa-file-archive-o"></i></div>
                                @break
                            @case('raz')
                                <div class="icon-file"><i class="fa fa-file-archive-o"></i></div>
                                @break
                            @default
                                <embed width="" height="200" src="{{ $file->path }}">
                                @break
                        @endswitch
                        <div class="card-body">
                            <p class="card-text p-0 m-0 card_name">
                                {{ $file->name }}
                            </p>
                        </div>
                        <div class="card-body">
                            <p class="card-text p-0 m-0">Thời gian thêm:
                                {{ date('d/m/Y H:i:s', strtotime($file->created_at)) }}
                            </p>
                            <p class="card-text p-0 m-0">Size: {{ $file->size }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="modal fade "
             tabindex="-1" role="dialog"
             aria-labelledby="modelTitleId" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <form method="post" class="form">
                    @csrf
                    @method('put')
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Thông tin file</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-8">
                                    <div class="form-group">
                                        <label for="">Dung lượng file: <span class="size_file"></span></label>
                                    </div>
                                </div>
                                <div class="col-4">
                                    <a class="btn btn-dark download">Tải xuống</a>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Thời đăng tải:
                                    <span class="time"></span>
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="">Tên file</label>
                                <input type="text" name="name"
                                       class="form-control" placeholder="" aria-describedby="helpId">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát
                            </button>
                            <button type="submit" class="btn btn-primary">Lưu</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div id="contextMenu" class="dropdown">
            <div id="menu">
                <ul>
                    <li>
                        <a id="view" data-fslightbox href="/storage/files/MicrosoftTeams-imag.png">View</a>
                    </li>
                    <li data-toggle="modal" id="detail_info"><a>Detail</a></li>
                    <li id="rename"><a>Rename</a></li>
                    <li><a tabindex="-1" href="#" class="download">Download</a></li>
                    <li><a href="#" class="copy_file">Make copy file</a></li>
                </ul>
            </div>
        </div>
    </div>

    {{--    popup rename--}}
    <div>
        <div class="popup" id="myPopup">
            <div class="popup-wrapper">
                <button class="popup_button" type="submit">X</button>
                <div class="popup_content">
                    <form method="post" class="form">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="">Rename file</label>
                            <input type="text" name="name"
                                   class="form-control" placeholder="" aria-describedby="helpId">
                        </div>
                        <p class="error red-text custom-text-validate" id="overview_title_error"></p>
                        <p class="t-center">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- popup move file--}}
    <div>
        <div class="popup" id="movePopup">
            <div class="popup-wrapper">
                <button class="popup_button" type="submit">X</button>
                <div class="popup_content">
                    <form action="{{route('move')}}" method="post" id="form_move">
                        @csrf
                        <div class="form-group">
                            <label for="">Choose folder</label>
                            <select name="id_folder_move" class="form-control">
                                @foreach($folderMove as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                            <input type="hidden" name="file_id" id="move_file">
                        </div>
                        <p class="error red-text custom-text-validate" id="overview_title_error"></p>
                        <p class="t-center">
                            <button type="submit" class="btn btn-primary">Move</button>
                        </p>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')
    <script src="https://cdn.jsdelivr.net/npm/resumablejs@1.1.0/resumable.min.js"></script>
    <script src="{{ asset('asset/js/system/folder/main.js') }}"></script>
    <script src="{{ asset('asset/js/system/folder/fslightbox.js') }}"></script>
    <script>
        main.uploadFileChunk('{{ route('file.upload', ['id' => $folder->id]) }}', '{{ csrf_token() }}');

        $(document).on('contextmenu', '.card_file', function (e) {
            let file = $(this).data('file');

            $('#detail_info').attr('data-target', '#modelFIleId_' + file.id);
            $('.modal').attr('id', 'modelFIleId_' + file.id);

            $('.size_file').text(file.size);
            $('.download').attr('href', file.path);
            $('.download').attr('download', file.name);
            $('.copy_file').data('id', file.id);
            $('.time').text(file.created_at);
            $("form input[type='text']").val(file.name);

            $("#view").attr('href', file.path);
            refreshFsLightbox();

            //$("#move_file").val(file.id);

            let url = $(this).data('url');
            $('.form').attr('action', url);


            $("#contextMenu").css({
                display: "block",
                left: e.pageX / 1.5,
                top: e.pageY / 1.5,
            });
            return false;
        });

        $(document).on('click', 'html', function () {
            $("#contextMenu").hide();
        })

        $(document).on('change',"input[type='checkbox']", function (){
            if ($("input:checked").length > 0) {
                $('.delete_file').css("display", "block");
                $('.move_file').css('display', 'block');
            } else {
                $('.delete_file').css("display", "none");
                $('.move_file').css('display', 'none');
            }
        });

        $(document).on('click', '.card_file', function () {
            let checkbox = $(this).find($("input[type='checkbox']"))
            if (checkbox.prop('checked')) {
                checkbox.prop('checked', false)
            } else {
                checkbox.prop('checked', true)
            }
            if ($("input:checked").length > 0) {
                $('.delete_file').css("display", "block");
                $('.move_file').css('display', 'block');
            } else {
                $('.delete_file').css("display", "none");
                $('.move_file').css('display', 'none');
            }
        });


        $(document).on('click', '.delete_file', function () {
            let arr = [];
            $("input.checkbox-select[type='checkbox']:checked").each(function () {
                let id = $(this).closest('.card_file').data('file').id;
                arr.push(id);
            });

            let url = '{{ route("file.delete") }}';
            if (arr.length > 0) {
                swal.fire({
                    title: 'Are you sure?',
                    html: 'You want to delete <b>(' + arr.length + ')</b> file',
                    showCancelButton: true,
                    showCloseButton: true,
                    confirmButtonText: 'Yes, Delete',
                    cancelButtonText: 'Cancel',
                    confirmButtonColor: '#556ee6',
                    cancelButtonColor: '#d33',
                    width: 300,
                    allowOutsideClick: false
                }).then(function (result) {
                    if (result.value) {
                        $.post(url, {
                            file_ids: arr
                        }, function (data) {
                            if (data.code == 200) {
                                toastr.success(data.msg);
                                setTimeout(function () {// wait for 5 secs(2)
                                    location.reload(); // then reload the page.(3)
                                }, 100);
                            }
                        }, 'json');
                    }
                })
            }
        })

        $("#search").on('keyup', function () {
            let value = $(this).val();
            $.ajax({
                type: 'get',
                url: '{{route('search')}}',
                data: {
                    value: value,
                    id: {{$id}}
                },
                success: function (result) {
                    $("#counties-table").html(result.data);
                },
                error: function (err) {

                }
            });
        });

        $("#rename").click(function () {
            $("#myPopup").show();
        });

        $("#move").click(function () {
            $("#movePopup").show();
        });

        $(document).on('click', '.move_file', function () {
            $('#movePopup').show();

            let arr = [];
            $("input.checkbox-select[type='checkbox']:checked").each(function () {
                let id = $(this).closest('.card_file').data('file').id;
                arr.push(id);
            });

            $("#move_file").val(arr);
        })

        $(".popup_button").click(function () {
            $(".popup").hide();
        })


        var popup = document.getElementById("myPopup");
        window.onclick = function (event) {
            if (event.target == popup) {
                popup.style.display = "none";
            }
        }

        $(document).on('click', ".copy_file", function () {
            let id = $('.copy_file').data('id');
            $.ajax({
                type: 'post',
                url: '{{route('copy')}}',
                data: {
                    idFolder: {{$folder->id}},
                    id: id,
                },
                success: function (result) {
                    setTimeout(function () {// wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 100);
                },
                error: function (err) {

                }
            });
        })

    </script>
@endsection
