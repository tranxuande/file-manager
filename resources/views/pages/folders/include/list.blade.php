@if (count($dataParent->childrens))
    @php
        $dash .= '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-dash-lg" viewBox="0 0 16 16">
  <path fill-rule="evenodd" d="M2 8a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11A.5.5 0 0 1 2 8Z"/>
</svg>';
    @endphp
    @foreach ($dataParent->childrens as $dataChild)
        <tr class="">

            <td scope="row">{!! $dash !!} {{ $dataChild->name }}</td>
            <td>
                <div class="dropdown mr-1">
                    <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown"
                        aria-expanded="false" data-offset="10,20">
                        Thao tác
                    </button>
                    <div class="dropdown-menu">
                        <button type="button" class="dropdown-item" data-toggle="modal"
                            data-target="#modelFolderAdd_{{ $dataChild->id }}">
                            Thêm thư mục con
                        </button>
                        <button type="button" class="dropdown-item" data-toggle="modal"
                            data-target="#modelFolderEdit_{{ $dataChild->id }}">
                            Sửa
                        </button>
                        <a class="dropdown-item" href="#">Xóa</a>
                        <a class="dropdown-item" href="{{ route('folder.show', ['id' => $dataChild->id]) }}">Xem thư mục</a>
                    </div>
                </div>
                <div class="modal fade" id="modelFolderAdd_{{ $dataChild->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="modelTitleId" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-scrollable" tabindex="-1" data-bs-backdrop="static"
                        data-bs-keyboard="false" role="dialog" aria-labelledby="modalTitleId" aria-hidden="true"
                        role="document">
                        <div class="modal-content">
                            <form action="{{ route('folder.store', ['id' => $dataChild->id]) }}" method="post">
                                @csrf
                                <div class="modal-header">
                                    <h5 class="modal-title">Thư mục : {{ $dataChild->name }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="">Tên thư mục</label>
                                        <input type="text" class="form-control" name="name" id=""
                                            aria-describedby="helpId" placeholder="">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                                    <button type="submit" class="btn btn-primary">Lưu</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="modal fade" id="modelFolderEdit_{{ $dataChild->id }}" tabindex="-1" role="dialog"
                    aria-labelledby="modelTitleId" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form action="{{ route('folder.update', $dataChild->id) }}" method="post">
                                @csrf
                                @method('put')
                                <div class="modal-header">
                                    <h5 class="modal-title">Sửa thư mục : {{ $dataChild->name }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <label for="">Tên thư mục</label>
                                        <input type="text" class="form-control" value="{{ $dataChild->name }}"
                                            name="name" id="" aria-describedby="helpId" placeholder="">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Thoát</button>
                                    <button type="submit" class="btn btn-primary">Lưu</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </td>
        </tr>
        @if (count($dataChild->childrens))
            @include('pages.folders.include.list', [
                'dataParent' => $dataChild,
            ])
        @endif
    @endforeach
@endif
