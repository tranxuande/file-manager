@if(!empty($searchFile))
    @foreach ($searchFile as $file)
        <div class="col-3 card_file" data-file="{{$file}}" data-url="{{route('file.rename',$file->id)}}">
            <div class=" card  text-white bg-info card-file ">
                <input type="checkbox">
                @switch($file->type_file)
                    @case('doc')
                        <div class="icon-file"><i class="fa fa-file-word"></i></div>
                        @break
                    @case('docx')
                        <div class="icon-file"><i class="fa fa-file-word"></i></div>
                        @break
                    @case('xlsx')
                        <div class="icon-file"><i class="fa fa-solid fa-file-excel"></i></div>
                        @break
                    @case('zip')
                        <div class="icon-file"><i class="fa fa-file-archive-o"></i></div>
                        @break
                    @case('raz')
                        <div class="icon-file"><i class="fa fa-file-archive-o"></i></div>
                        @break
                    @default
                        <embed width="" height="200" src="{{ $file->path }}">
                        @break
                @endswitch
                <div class="card-body">
                    <p class="card-text p-0 m-0 card_name">
                        {{ $file->name }}
                    </p>
                </div>
                <div class="card-body">
                    <p class="card-text p-0 m-0">Thời gian thêm:
                        {{ date('d/m/Y H:i:s', strtotime($file->created_at)) }}
                    </p>
                    <p class="card-text p-0 m-0">Size: {{ $file->size }}</p>
                </div>

            </div>
        </div>
    @endforeach
@endif
