@extends('user.layouts.master')
@section('title', 'Quản lý thư mục')
@section('content_title', 'Danh sách thư mục')
@section('content')
    <link href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="{{asset('asset/css/treeSortable.css')}}" rel="stylesheet" />
    <div class="card">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table class="table table-striped   table-hover   table-borderless table-primary  align-middle"
                           id="tblLocations">
                        <thead class="table-light">
                        <tr>
                            <th>Tên thư mục</th>
                            <th>Thao tác</th>
                        </tr>
                        </thead>
                        <tbody class="table-group-divider">
                        @php
                            $dash = '';
                        @endphp
                        @foreach ($datas as $data)
                            <tr class="">

                                <td scope="row">{{ $data->name }}</td>
                                <td>
                                    <div class="dropdown mr-1">
                                        <button type="button" class="btn btn-secondary dropdown-toggle"
                                                data-toggle="dropdown" aria-expanded="false" data-offset="10,20">
                                            Thao tác
                                        </button>
                                        <div class="dropdown-menu">
                                            <button type="button" class="dropdown-item" data-toggle="modal"
                                                    data-target="#modelFolderAdd_{{ $data->id }}">
                                                Thêm thư mục con
                                            </button>
                                            <a class="dropdown-item"
                                               href="{{ route('folder.show', ['id' => $data->id]) }}">Xem thư mục</a>
                                        </div>
                                    </div>
                                    <div class="modal fade" id="modelFolderAdd_{{ $data->id }}" tabindex="-1"
                                         role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-scrollable" tabindex="-1"
                                             data-bs-backdrop="static" data-bs-keyboard="false" role="dialog"
                                             aria-labelledby="modalTitleId" aria-hidden="true" role="document">
                                            <div class="modal-content">
                                                <form action="{{ route('folder.store', ['id' => $data->id]) }}"
                                                      method="post">
                                                    @csrf
                                                    <div class="modal-header">
                                                        <h5 class="modal-title">Thư mục : {{ $data->name }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <label for="">Tên thư mục</label>
                                                            <input type="text" class="form-control" name="name"
                                                                   id="" aria-describedby="helpId" placeholder="">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">Thoát
                                                        </button>
                                                        <button type="submit" class="btn btn-primary">Lưu</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @include('pages.folders.include.list', [
                                'dataParent' => $data,
                            ])
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page-script')

@endsection

