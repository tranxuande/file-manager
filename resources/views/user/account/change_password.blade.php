@extends('user.layouts.master')
@section('title', 'Thietkewebsite86 | Đổi Mật Khẩu')
@section('content')
    @include('user.layouts.alert')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-cyan">
                <div class="card-header">
                    <h3 class="card-title">{{__('Đổi Mật Khẩu')}}</h3>
                </div>
                <form action="{{route('changePass')}}" method="post">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label>{{__('Mật khẩu mới')}}</label>
                            <input type="password" id="password" name="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>{{__('Nhập lại mật khẩu mới')}}</label>
                            <input type="password" name="password_confirmation" class="form-control">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-success">{{__('Thay đổi')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $('form').validate({
            rules: {
                current_password: {
                    required: true
                },
                password: {
                    required: true,
                },
                password_confirmation: {
                    required: true,
                    equalTo : "#password"
                }
            },
            messages: {
                current_password: {
                    required: "{{__('Mật khẩu hiện tại là bắt buộc')}}"
                },
                password: {
                    required: "{{__('Mật khẩu mới là bắt buộc')}}",
                },
                password_confirmation: {
                    required: "{{__('Nhập lại mật khẩu mới là bắt buộc')}}",
                    equalTo: "{{__('Mật khẩu nhập lại không khớp')}}"
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });
    </script>
@endsection
