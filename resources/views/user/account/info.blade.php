@extends('user.layouts.master')
@section('title', 'Thietkewebsite86 | Thông Tin Cá Nhân')
@section('content')
    @include('user.layouts.alert')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-cyan">
                <div class="card-header">
                    <h3 class="card-title">{{__('Thông Tin')}}</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>{{__('Email Của Bạn')}} :
                                    @if(\Illuminate\Support\Facades\Auth::guard('web')->user()->email_verified_at)
                                        <b style="color: green">{{__('Đã Xác Minh')}}</b>
                                    @else
                                        <b style="color: orange">{{__('Chưa Xác Minh')}}</b> <a style="color: blue" href="{{route('user.account.resendVerifyEmail')}}">({{__('Xác Minh Ngay')}})</a>
                                    @endif
                                </label>
                                <input type="text" value="{{\Illuminate\Support\Facades\Auth::guard('web')->user()->email}}" class="form-control" disabled>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
