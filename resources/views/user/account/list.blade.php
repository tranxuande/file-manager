@extends('user.layouts.master')
@section('title', 'Quản lý thư mục')
@section('content_title', 'Danh sách user')
@section('content')

    <table class="table">
        <thead>
        <tr>
            <th>STT</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
        </tr>
        </thead>
        <tbody>
        @php $i=1 @endphp
        @foreach($list as $item)
            <tr>
                <td>{{$i++}}</td>
                <td>{{$item->name}}</td>
                <td>{{$item->email}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endsection

@section('page-script')
@endsection
