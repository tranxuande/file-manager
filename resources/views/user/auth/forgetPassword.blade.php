@extends('user.auth.master')
@section('title', 'Thietkewebsite86.com | ResetPassword')
@section('content')
    <div class="login-box">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a class="logo navbar-brand" href="/">
                    <h1 class="m-0" style="font-size: 30px"><i class="fa fa-laptop-code me-2"></i>Thietkewebsite86</h1>
                </a>
            </div>
            @if(Session::has('message'))
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <p>{{ Session::get('message') }}</p>
                    <button type="button" class="close" data-dismiss="alert" aria-label="{{__('Đóng')}}">
                        <span aria-hidden="true">&times;</span>
                        <span class="sr-only">{{__('Đóng')}}</span>
                    </button>
                </div>
            @endif
            <div class="card-body">
                <p class="login-box-msg">{{__('Reset password')}}</p>
                <form method="post" action="{{ route('forget.password.post')}}">
                    @csrf
                    <div class="form-group input-group mb-3">
                        <input type="email" class="form-control" placeholder="Email" name="email" value="{{old('email')}}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-5">
                            <a class="btn btn-primary btn-block" onclick="login()">{{__('Reset password')}}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $('form').validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
            },
            messages: {
                email: {
                    required: "{{__('Vui lòng nhập email')}}",
                    email: "{{__('Email không đúng định dạng')}}",
                },
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });

        function login() {
            $('form').submit();
        }
    </script>
@endsection
