@extends('user.auth.master')
@section('title', 'Thietkewebsite86.com | Đăng nhập')
@section('content')
    <div class="login-box">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a class="logo navbar-brand" href="/">
                    <h1 class="m-0" style="font-size: 30px"><i class="fa fa-laptop-code me-2"></i>Thietkewebsite86</h1>
                </a>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="{{__('Đóng')}}">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">{{__('Đóng')}}</span>
                        </button>
                    </div>
                @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <p>{{ Session::get('error') }}</p>
                        <button type="button" class="close" data-dismiss="alert" aria-label="{{__('Đóng')}}">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">{{__('Đóng')}}</span>
                        </button>
                    </div>
                @endif
                <p class="login-box-msg">{{__('Đăng nhâp để đến trang quản trị')}}</p>
                <form action="{{route('login')}}" method="post">
                    @csrf
                    <div class="form-group input-group mb-3">
                        <input type="email" class="form-control" placeholder="Email" name="email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group input-group mb-3">
                        <input type="password" class="form-control" placeholder="Password" name="password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-7">
                            <div class="icheck-primary">

                            </div>
                        </div>
                        <div class="col-5">
                            <a class="btn btn-primary btn-block" onclick="login()">{{__('Đăng nhập')}}</a>
                        </div>
                    </div>
                </form>
                <p class="mb-1">
                    <a href="{{route('forget.password.get')}}">{{__('Bạn quên mật khẩu?')}}</a>
                </p>
                <p class="mb-0">
                    <a href="{{route('register')}}" class="text-center">{{__('Tạo tài khoản mới')}}</a>
                </p>
            </div>
        </div>
    </div>
    <script>
        $('form').validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                }
            },
            messages: {
                email: {
                    required: "{{__('Vui lòng nhập email')}}",
                    email: "{{__('Email không đúng định dạng')}}",
                },
                password: {
                    required: "{{__('Vui lòng nhập mật khẩu')}}",
                }
            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });

        function login(){
            $('form').submit();

            // if(grecaptcha.getResponse(widgetId1)){
            //     $('form').submit()
            // }else{
            //     $('#error_captcha').html('<span class="error invalid-feedback" style="display: block">Vui lòng check vào captcha.</span>')
            // }
        }
    </script>
@endsection
