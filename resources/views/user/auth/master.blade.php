<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', 'Thietkewebsite86.com | Chuyên thiết kế website chuyên ngiệp, thiết kế website dịch vụ, phát triển tính năng theo yêu cầu')</title>
    <link href="{{asset('image/web/icon.png')}}" rel="icon"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{asset('adminlte/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminlte/icheck-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('adminlte/adminlte.min.css')}}">

    <script type="text/javascript">
        var onloadCallback = function() {
            widgetId1 = grecaptcha.render('html_element', {
                'sitekey' : '6LceEhAdAAAAAOqf-A8zGLICJtsGiib-jnAFH9ka'
            });
        };
    </script>
</head>
<body class="hold-transition login-page">
<script src="{{asset('adminlte/jquery.min.js')}}"></script>
<script src="{{asset('adminlte/bootstrap.bundle.min.js')}}"></script>
<script src="{{asset('adminlte/adminlte.min.js')}}"></script>
<script src="{{asset('adminlte/jquery.validate.min.js')}}"></script>
@yield('content')
<script src="https://www.google.com/recaptcha/api.js?hl=en&onload=onloadCallback&render=explicit"
        async defer>
</script>
</body>
</html>
