@extends('user.auth.master')
@section('title', 'Thietkewebsite86.com | Reset')
@section('content')
    <div class="login-box">
        <div class="card card-outline card-primary">
            <div class="card-header text-center">
                <a class="logo navbar-brand" href="/">
                    <h1 class="m-0" style="font-size: 30px"><i class="fa fa-laptop-code me-2"></i>Thietkewebsite86</h1>
                </a>
            </div>
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="{{__('Đóng')}}">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">{{__('Đóng')}}</span>
                        </button>
                    </div>
                @endif

                <p class="login-box-msg">{{__('Reset Password')}}</p>
                <form action="{{route('reset.password.post')}}" method="post">
                    @csrf
                    <div class="form-group input-group mb-3">
                        <input type="email" class="form-control" placeholder="Email" name="email"
                               value="{{ $email }}">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group input-group mb-3">
                        <input type="password" class="form-control" placeholder="Password" name="password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group input-group mb-3">
                        <input type="password" class="form-control" placeholder="Confirm Password"
                               name="password_confirmation">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-7">

                        </div>
                        <div class="col-5">
                            <a class="btn btn-primary btn-block" onclick="login()">{{__('Save')}}</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $('form').validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                },

            },
            messages: {
                email: {
                    required: "{{__('Vui lòng nhập email')}}",
                    email: "{{__('Email không đúng định dạng')}}",
                },
                password: {
                    required: "{{__('Vui lòng nhập mật khẩu')}}",
                },

            },
            errorElement: 'span',
            errorPlacement: function (error, element) {
                error.addClass('invalid-feedback');
                element.closest('.form-group').append(error);
            },
            highlight: function (element, errorClass, validClass) {
                $(element).addClass('is-invalid');
            },
            unhighlight: function (element, errorClass, validClass) {
                $(element).removeClass('is-invalid');
            }
        });

        function login() {
            $('form').submit();
        }
    </script>
@endsection
