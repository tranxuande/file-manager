<div class="row">
    <div class="col-md-12">
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible alertSlideUp">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!!$message!!}
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissible alertSlideUp">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!!$message!!}
            </div>
        @endif
        @if ($message = Session::get('warning'))
            <div class="alert alert-warning alert-dismissible alertSlideUp">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {!!$message!!}
            </div>
        @endif
    </div>
</div>
<script>
    $(".alertSlideUp").fadeTo(5000, 500).slideUp(500, function () {
        $(".alertSlideUp").slideUp(1000);
    });
</script>
