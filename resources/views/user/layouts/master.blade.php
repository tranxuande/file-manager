<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title', 'Thietkewebsite86')</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('image/web/icon.png') }}">
    <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <link rel="stylesheet" href="{{ asset('adminlte/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/pace-theme-flat-top.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/select2-bootstrap4.min.css') }}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ asset('adminlte/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/summernote-bs4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminlte/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('asset/css/main.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    @yield('page-style')
</head>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/616aa08c86aee40a5736dd96/1fi4869r9';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->

<body class="hold-transition sidebar-mini layout-fixed pace-primary">
    <script src="{{ asset('adminlte/jquery.min.js') }}"></script>
    <script src="{{ asset('adminlte/blockui.min.js') }}"></script>
    <script src="{{ asset('adminlte/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('adminlte/pace.min.js') }}"></script>
    <script src="{{ asset('adminlte/jquery.validate.min.js') }}"></script>
    <script src="{{ asset('adminlte/adminlte.js') }}"></script>
    <script src="{{ asset('adminlte/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('adminlte/select2.full.min.js') }}"></script>
    <script src="{{ asset('adminlte/bs-custom-file-input.min.js') }}"></script>
    <script src="{{ asset('adminlte/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('adminlte/toastr.min.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    <div class="wrapper">
        {{--        @include('user.layouts.preloader') --}}
        @include('user.layouts.header')
        @include('user.layouts.sidebar')
        <div class="content-wrapper">
            @section('content-header')
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                                <h1 class="m-0">@yield('content_title')</h1>
                            </div>
                            <div class="col-sm-6">
                                <ol class="breadcrumb float-sm-right">
                                    <li class="breadcrumb-item">Tin tức</li>
                                    <li class="breadcrumb-item active">Bài viêt</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            @show
            <section class="content">
                <div class="container-fluid">
                    @yield('content')
                </div>
            </section>
        </div>
    </div>


    @error(config('util.SUCCESS'))
        <script>
            toastr.success("{{ $message }}");
        </script>
    @enderror
    @error(config('util.ERROR'))
        <script>
            toastr.error("{{ $message }}");
        </script>
    @enderror
    @if (session()->has(config('util.ERROR')))
        <script>
            toastr.error("{{ session()->get(config('util.ERROR')) }}");
        </script>
    @endif
    @if (session()->has(config('util.SUCCESS')))
        <script>
            toastr.success("{{ session()->get(config('util.SUCCESS')) }}");
        </script>
    @endif
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    </script>
    @yield('page-script')
</body>

</html>
