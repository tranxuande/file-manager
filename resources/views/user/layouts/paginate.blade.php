@if ($paginator->hasPages())
    <ul class="pagination pagination-sm m-0 float-right">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled"><a class="page-link" href="#">&laquo;</a></li>
        @else
            <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}">&laquo;</a></li>
        @endif

        {{-- Pagination --}}
        @for ($i=1; $i<=$paginator->lastPage(); $i++)
            @if ($i == $paginator->currentPage())
                <li class="page-item active"><span class="page-link">{{ $i }}</span></li>
            @elseif ($i > 1 && $i == $paginator->currentPage() - 2)
                <li class="page-item disabled"><span class="page-link"><i class="fa fa-ellipsis-h"></i></span></li>
            @elseif ($i == 1 || ($i == $paginator->currentPage() + 1 || $i == $paginator->currentPage() - 1)  || $i == $paginator->lastPage())
                <li><a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a></li>
            @elseif ($i == $paginator->lastPage() - 1)
                <li class="page-item disabled"><span class="page-link"><i class="fa fa-ellipsis-h"></i></span></li>
            @endif
        @endfor

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}">&raquo;</a></li>
        @else
            <li class="page-item disabled"><a class="page-link" href="#">&raquo;</a></li>
        @endif
    </ul>
@endif
