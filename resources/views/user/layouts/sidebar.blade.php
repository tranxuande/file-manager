@php
    $route = Request::route()->getName();
@endphp
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('adminlte/img/avatar5.png') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#"
                    class="d-block">{{ \Illuminate\Support\Facades\Auth::guard('web')->user()->name ?? '' }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item">
                    <a href="{{ route('folder.list') }}" class="nav-link ">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                            class="bi bi-file-earmark-fill" viewBox="0 0 16 16">
                            <path
                                d="M4 0h5.293A1 1 0 0 1 10 .293L13.707 4a1 1 0 0 1 .293.707V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2zm5.5 1.5v2a1 1 0 0 0 1 1h2l-3-3z" />
                        </svg>
                        <p>
                            {{ __('Thư mục của bạn') }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link ">
                        <i class="nav-icon fas fa-desktop"></i>
                        <p>
                            {{ __('Kho giao diện') }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="" class="nav-link ">
                        <i class="nav-icon fas fa-receipt"></i>
                        <p>
                            {{ __('Hóa đơn & thanh toán') }}

                        </p>
                    </a>
                </li>
                <li class="nav-header">{{ __('Người Dùng') }}</li>

                @role('admin')
                <li class="nav-item">
                    <a href="{{route('list.user')}}" class="nav-link ">
                        <i class="nav-icon fas fa-user-edit"></i>
                        <p>
                            {{ __('Thông tin user') }}
                        </p>
                    </a>
                </li>
                @endrole

                <li class="nav-item">
                    <a href="{{route('changePass')}}" class="nav-link ">
                        <i class="nav-icon fas fa-key"></i>
                        <p>
                            {{ __('Đổi mật khẩu') }}
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('user.logout') }}" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>
                            {{ __('Đăng xuất') }}
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
