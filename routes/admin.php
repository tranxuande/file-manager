<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FileController;
use App\Http\Controllers\FolderController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth.login')->group(function () {
    Route::namespace('Admin')->prefix('folder')->group(function () {
        Route::get('', [FolderController::class, 'index'])->name('folder.list');
        Route::post('{id}', [FolderController::class, 'store'])->name('folder.store');
        Route::put('{id}', [FolderController::class, 'update'])->name('folder.update');
        Route::get('{id}', [FolderController::class, 'show'])->name('folder.show');
        Route::get('user', "UserController@getAll()")->name('list.user');
    });

    Route::get('user', [\App\Http\Controllers\User\UserController::class, 'getAll'])->name('list.user');
    Route::get('changePass', [\App\Http\Controllers\User\ChangePassController::class, 'showFormChangePass'])->name('changePass');
    Route::post('changePass', [\App\Http\Controllers\User\ChangePassController::class, 'postFormChangePass'])->name('changePass');
    Route::namespace('Admin')->prefix('file')->group(function () {
        Route::prefix('{id}')->group(function () {
            Route::post('save', [FileController::class, 'uploadFile'])->name('file.upload');
            Route::put('rename', [FileController::class, 'renameFile'])->name('file.rename');

        });
        Route::post('delete', [FileController::class, 'deleteFile'])->name('file.delete');
        Route::get('search', [FileController::class, 'search'])->name('search');
        Route::post('copy', [FileController::class, 'copy'])->name('copy');
        Route::post('move', [FileController::class, 'move'])->name('move');
    });
});
//Route::namespace('Admin')->prefix('folder')->middleware(['auth.login'])->group(function () {
//    Route::get('', [FolderController::class, 'index'])->name('folder.list');
//    Route::post('{id}', [FolderController::class, 'store'])->name('folder.store');
//    Route::put('{id}', [FolderController::class, 'update'])->name('folder.update');
//    Route::get('{id}', [FolderController::class, 'show'])->name('folder.show');
//    Route::get('user',"UserController@getAll()")->name('list.user');
//});
//
//Route::get('user',[\App\Http\Controllers\User\UserController::class,'getAll'])->name('list.user');
//Route::get('changePass',[\App\Http\Controllers\User\ChangePassController::class,'showFormChangePass'])->middleware('auth.login')->name('changePass');
//Route::post('changePass',[\App\Http\Controllers\User\ChangePassController::class,'postFormChangePass'])->name('changePass');
//Route::namespace('Admin')->prefix('file')->group(function () {
//    Route::prefix('{id}')->group(function () {
//        Route::post('save', [FileController::class, 'uploadFile'])->name('file.upload');
//        Route::put('rename', [FileController::class, 'renameFile'])->name('file.rename');
//
//    });
//    Route::post('delete', [FileController::class, 'deleteFile'])->name('file.delete');
//    Route::get('search',[FileController::class, 'search'])->name('search');
//    Route::post('copy',[FileController::class, 'copy'])->name('copy');
//    Route::post('move',[FileController::class, 'move'])->name('move');
//});
