<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::namespace('Web')->group(function () {
//    // Route::get('/', 'HomeController@index')->name('web.home.index');
//    Route::get('/',  function () {
//        return view('user.layouts.master');
//    });
//});

Route::namespace('User')->group(function () {
    Route::get('/logout', 'AuthController@logout')->name('user.logout');
    Route::get('/', 'AuthController@showLoginForm')->name('login');
    Route::post('/', 'AuthController@login')->name('login');
    Route::get('/register', 'AuthController@showRegisterForm')->name('register');
    Route::post('/register', 'AuthController@register')->name('register');

    Route::get('forget-password', 'AuthController@showForgetPasswordForm')->name('forget.password.get');
    Route::post('forget-password', 'AuthController@submitForgetPasswordForm')->name('forget.password.post');
    Route::get('reset-password/{email}', 'AuthController@showResetPasswordForm')->name('reset.password.get');
    Route::post('reset-password', 'AuthController@submitResetPasswordForm')->name('reset.password.post');

});
